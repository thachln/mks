/*
 * Filename: StudentTest.java
 *
 * Version: v1.0
 *
 * Date: Oct, 22 2008
 *
 * Copyright notice
 *
 * Modification Logs:
 *      DATE        AUTHOR      DESCRIPTION
 *  ------------------------------------------------------------------
 *  22-Oct-2008     TranNTB     Create file.
 */
/*===================================================================*/
package vn.fpt.fsoft.hibernateDemo.util;

import java.util.List;


import org.hibernate.Session;
import org.hibernate.SessionFactory;

import vn.fpt.fsoft.hibernateDemo.entity.Student;

import junit.framework.TestCase;

/**
 * StudentTest class to test object exist into database.
 */
public class StudentTest extends TestCase {

    /**
     * To test object exist.
     */
    public final void testQuery1() {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List listStu = session.createQuery(" from Student").list();
        assertEquals(3, listStu.size());
        session.getTransaction().commit();
    }

    /**
     * To test object exist.
     */
    public final void testQuery2() {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List < Student > listStu = session.createQuery(
                "select stu from Student as stu where stu.id=:idStu")
                .setString("idStu", "01").list();
        assertEquals(listStu.get(0).getId(), "01");
        session.getTransaction().commit();
    }
}
