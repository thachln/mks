/*
 * Filename: Student.java
 *
 * Version: v1.0
 *
 * Date: Oct, 22 2008
 *
 * Copyright notice
 *
 * Modification Logs:
 *      DATE        AUTHOR      DESCRIPTION
 *  ------------------------------------------------------------------
 *  22-Oct-2008     TranNTB     Create file.
 */
/*===================================================================*/
package vn.fpt.fsoft.hibernateDemo.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * Student class contains data model of student.
 */
public class Student {

    /**
     * Declare String id.
     */
    private String id;

    /**
     * Declare String name.
     */
    private String name;

    /**
     * Declare set < String > emails.
     */
    private Set < String > emails;

    /**
     * No-argument constructor is required for mapping.
     */
    public Student() {
    }

    /**
     * Constructor with 2 argument.
     * @param stuId type String.
     * @param stuName type String.
     */
    public Student(final String stuId, final String stuName) {
        this.id = stuId;
        this.name = stuName;
    }

    /**
     * Getter id.
     * @return id property.
     */
    public final String getId() {
        return id;
    }

    /**
     * Setter id.
     * @param stuId type String.
     */
    public final void setId(final String stuId) {
        this.id = stuId;
    }

    /**
     * Getter name.
     * @return name property.
     */
    public final String getName() {
        return name;
    }

    /**
     * Setter name.
     * @param stuName type String.
     */
    public final void setName(final String stuName) {
        this.name = stuName;
    }

    /**
     * Getter emails.
     * @return emails property.
     */
    public final Set < String > getEmails() {
        return emails;
    }

    /**
     * Setter emails.
     * @param stuEmails type Set < String >.
     */
    public final void setEmails(final Set < String > stuEmails) {
        this.emails = stuEmails;
    }

    /**
     * Setter emails.
     * @param stuEmails type Set < String >.
     */
    public final void setEmails(final String... stuEmails) {
        Set < String > mails = new HashSet < String > ();
        for (String i : stuEmails) {
            mails.add(i);
        }
        this.emails = mails;
    }
}
