/*
 * Filename: Create.java
 *
 * Version: v1.0
 *
 * Date: Oct, 22 2008
 *
 * Copyright notice
 *
 * Modification Logs:
 *      DATE        AUTHOR      DESCRIPTION
 *  ------------------------------------------------------------------
 *  22-Oct-2008     TranNTB     Create file.
 */
/*===================================================================*/
package vn.fpt.fsoft.hibernateDemo.util;

import org.hibernate.Session;

import vn.fpt.fsoft.hibernateDemo.entity.Student;

/**
 * Create class to insert object to database.
 */
public final class Create {

    /**
     * Declare private constructor.
     */
    private Create() {
    }

    /**
     * Main to insert object into database.
     * @param args type String[].
     */
    public static void main(final String[] args) {

        // Create student 'nam'
        Student nam = new Student("01", "nguyen van nam");
        nam.setEmails("nam@yahoo.com", "nam13@gmail.com");

        // Create student 'bao'
        Student bao = new Student("02", "nguyen van bao");
        bao.setEmails("baonv@fsoft.com.vn");

        // Create student 'phuong'
        Student phuong = new Student("03", "nguyen thi phuong");
        phuong.setEmails("phuong@yahoo.com");

        // insert into database
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.save(nam);
        session.save(bao);
        session.save(phuong);        
        session.getTransaction().commit();
        
    }
}
